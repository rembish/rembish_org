from sqlalchemy.exc import SQLAlchemyError

from ..libraries.database import db


class CheckResult:
    def __init__(self, name, status=None):
        self.name = name
        self.status = status

    def __bool__(self):
        return self.status is None or self.status is True

    def export(self):
        return {
            "name": self.name,
            "status": self.status or "ok"
        }


class HealthCheck:
    def __init__(self):
        self.results = {}

    def process_db(self) -> (None, str):
        try:
            with db.engine.connect() as connection:
                connection.execute("SELECT 1")
                return None
        except SQLAlchemyError as ext:
            return str(ext)

    def process(self) -> bool:
        for name in dir(self):
            method = getattr(self, name)
            if not callable(method) or not name.startswith("process_"):
                continue
            check = name.lstrip("process_")
            self.results[check] = CheckResult(check, method())

        return all(self.results.values())

    def export(self) -> list:
        result = []
        for check in self.results.values():
            result.append(check.export())
        return result
