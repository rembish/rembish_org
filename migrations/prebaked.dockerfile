FROM mysql:8-debian AS builder

# That file does the DB initialization but also runs mysql daemon, by removing the last line it will only init
RUN sed -i 's/exec "$@"/echo "Skipping: $@"/' /usr/local/bin/docker-entrypoint.sh \
    && printf "[mysqld]\ncharacter-set-server = 'utf8mb4'\ncollation-server = 'utf8mb4_unicode_ci'" > /etc/mysql/conf.d/encoding.cnf \
    && printf "[mysqld]\ndefault-authentication-plugin = 'mysql_native_password'" > /etc/mysql/conf.d/authentication.cnf

# needed for intialization
ENV MYSQL_ROOT_PASSWORD=root
ENV MYSQL_DATABASE=rembish_org

COPY initial.sql /docker-entrypoint-initdb.d/

# Need to change the datadir to something else that /var/lib/mysql because the parent docker file defines it as a volume.
# https://docs.docker.com/engine/reference/builder/#volume :
#       Changing the volume from within the Dockerfile: If any build steps change the data within the volume after
#       it has been declared, those changes will be discarded.
RUN ["/usr/local/bin/docker-entrypoint.sh", "mysqld", "--datadir", "/initialized-db"]

FROM mysql:8-debian

ARG BUILD_DATE
ARG BUILD_HOSTNAME
ARG BUILD_JOB_NAME
ARG BUILD_NUMBER
ARG CI_COMMIT_TAG
ARG VCS_REF
ARG VCS_BRANCH
ARG VCS_TAG
ARG VERSION
ARG BUILD_TYPE="manual"

COPY --from=builder /etc/mysql /etc/mysql
COPY --from=builder /initialized-db /var/lib/mysql
COPY migrations/prebaked.dockerfile /Dockerfile

LABEL org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Alex Rembish" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-type="$BUILD_TYPE" \
      org.label-schema.build-host-name="$BUILD_HOSTNAME" \
      org.label-schema.build-ci-build-id="$BUILD_NUMBER" \
      org.label-schema.version="$VERSION" \
      org.label-schema.vcs-url="git@gitlab.com:rembish/rembish_org.git" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.docker.dockerfile="/Dockerfile" \
      org.label-schema.description="Rembish.org prebaked DB image" \
      org.label-schema.usage="https://gitlab.com/rembish/rembish_org/-/blob/master/README.md" \
      org.label-schema.url="https://gitlab.com/rembish/rembish_org" \
      org.label-schema.docker.cmd="docker run -d -p 3306:3306"
