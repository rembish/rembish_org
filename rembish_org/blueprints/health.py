from flask import Blueprint

from ..libraries.health import HealthCheck
from ..libraries.templating import with_json

root = Blueprint('health', __name__)


@root.route("/liveness")
@with_json
def liveness():
    return {}


@root.route("/readiness")
@with_json
def readiness():
    checker = HealthCheck()
    if checker.process():
        return {}

    return {
        "status": 504,
        "details": checker.export()
    }
