from os import environ

env = environ.get


class Configuration:
    SECRET_KEY = env("SECRET_KEY")

    SQLALCHEMY_DATABASE_URI = env("DATABASE_URL")
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_POOL_RECYCLE = 28799

    ME_NAME = "Alex Rembish"
    ME_EMAIL = "alex@rembish.org"

    TELEGRAM_TOKEN = env("TELEGRAM_TOKEN")
    TELEGRAM_CHAT_ID = env("TELEGRAM_CHAT_ID")

    GOOGLE_API_KEY = env("GOOGLE_API_KEY")
    GOOGLE_OAUTH_CLIENT_ID = env("GOOGLE_OAUTH_CLIENT_ID")
    GOOGLE_OAUTH_CLIENT_SECRET = env("GOOGLE_OAUTH_CLIENT_SECRET")

    SECURITY_CONFIRMABLE = False
    SECURITY_REGISTERABLE = False
    SECURITY_RECOVERABLE = False
    SECURITY_TRACKABLE = False
    SECURITY_PASSWORDLESS = True
    SECURITY_CHANGEABLE = False
    SECURITY_PASSWORD_SALT = env("PASSWORD_SALT")

    GEONAMES_USERNAME = env("GEONAMES_USERNAME")


class DevelopmentConfiguration(Configuration):
    SQLALCHEMY_DATABASE_URI = f"mysql+pymysql://{env('MYSQL_USER', 'alex')}:{env('MYSQL_PASSWORD')}@" \
                              f"{env('MYSQL_HOST')}/{env('MYSQL_DATABASE', 'rembish_org')}"
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_POOL_RECYCLE = 599

    DEBUG_TB_INTERCEPT_REDIRECTS = False


class ProductionConfiguration(Configuration):
    pass
