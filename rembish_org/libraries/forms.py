from json import dumps, loads

from wtforms import SelectField, SelectMultipleField, StringField


class LazySelectField(SelectField):
    def __init__(self, *, choices=None, **kwargs):
        super().__init__(choices=None, **kwargs)

        if callable(choices):
            self.choices = choices

    def iter_choices(self):
        if callable(self.choices):
            self.choices = self.choices()
        yield from super().iter_choices()


class LazySelectMultipleField(SelectMultipleField):
    def __init__(self, *, choices=None, **kwargs):
        super().__init__(choices=None, **kwargs)

        if callable(choices):
            self.choices = choices

    def iter_choices(self):
        if callable(self.choices):
            self.choices = self.choices()
        yield from super().iter_choices()

    def pre_validate(self, form):
        if callable(self.choices):
            self.choices = self.choices()
        super().pre_validate(form)


class JSONField(StringField):
    # pylint: disable=attribute-defined-outside-init
    def _value(self):
        return dumps(self.data) if self.data else ''

    def process_formdata(self, valuelist):
        if valuelist:
            try:
                self.data = loads(valuelist[0])
            except ValueError as exc:
                raise ValueError('This field contains invalid JSON') from exc
        else:
            self.data = None

    def pre_validate(self, form):
        super().pre_validate(form)
        if self.data:
            try:
                dumps(self.data)
            except TypeError as exc:
                raise ValueError('This field contains invalid JSON') from exc
