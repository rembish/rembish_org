FROM python:3.10-bullseye AS builder

WORKDIR /build
COPY poetry.lock pyproject.toml ./
RUN pip install poetry && poetry export --without-hashes --output requirements.txt && poetry export --without-hashes --dev --output dev-requirements.txt

FROM python:3.10-bullseye AS core

WORKDIR /app
COPY --from=builder /build/requirements.txt ./
COPY rembish_org ./rembish_org/
COPY uwsgi/uwsgi.ini ./
COPY Dockerfile /
RUN pip install -r requirements.txt && rm requirements.txt && rm -rf rembish_org/static

EXPOSE 5000
CMD ["uwsgi", "--ini", "uwsgi.ini", "--socket", ":5000", "--module", "rembish_org.application:create_app()"]

FROM core AS development

COPY --from=builder /build/dev-requirements.txt ./
RUN pip install -r dev-requirements.txt && rm dev-requirements.txt

FROM core

ARG BUILD_DATE
ARG BUILD_HOSTNAME
ARG BUILD_JOB_NAME
ARG BUILD_NUMBER
ARG CI_COMMIT_TAG
ARG VCS_REF
ARG VCS_BRANCH
ARG VCS_TAG
ARG VERSION
ARG BUILD_TYPE="manual"

LABEL org.label-schema.schema-version="1.0.0-rc.1" \
      org.label-schema.vendor="Alex Rembish" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.build-type="$BUILD_TYPE" \
      org.label-schema.build-host-name="$BUILD_HOSTNAME" \
      org.label-schema.build-ci-build-id="$BUILD_NUMBER" \
      org.label-schema.version="$VERSION" \
      org.label-schema.vcs-url="git@gitlab.com:rembish/rembish_org.git" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.docker.dockerfile="/Dockerfile" \
      org.label-schema.description="Rembish.org uWSGI image" \
      org.label-schema.usage="https://gitlab.com/rembish/rembish_org/-/blob/master/README.md" \
      org.label-schema.url="https://gitlab.com/rembish/rembish_org" \
      org.label-schema.docker.cmd="docker run -p 5000:5000 -d"
